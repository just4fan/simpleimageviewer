package com.horrrnet.simpleimageviewer

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.PointF
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.graphics.minus
import androidx.core.graphics.values
import com.horrrnet.simpleimageviewer.customscalelistener.CustomScaleGestureDetector
import com.horrrnet.simpleimageviewer.customscalelistener.CustomScaleGestureDetector.SimpleCustomOnScaleGestureListener
import kotlin.math.min

@SuppressLint("ClickableViewAccessibility")
class ZoomableImageView(
    context: Context,
    attrs: AttributeSet
) : View(context, attrs) {
    private val scaleListener = CustomScaleGestureDetector(context, ScaleListener())

    private var bitmap: Bitmap? = null

    private var transformationMatrix = Matrix()
    private var initialScale = 1f

    fun setImageBitmap(bm: Bitmap) {
        bitmap = bm

        post {
            val viewWidth = width.toFloat()
            val viewHeight = height.toFloat()

            val bmpWidth = bm.width.toFloat()
            val bmpHeight = bm.height.toFloat()

            initialScale = min(viewWidth / bmpWidth, viewHeight / bmpHeight)

            val initXOffset = (viewWidth - bmpWidth * initialScale) / 2f
            val initYOffset = (viewHeight - bmpHeight * initialScale) / 2f

            transformationMatrix.setScale(initialScale, initialScale)
            transformationMatrix.postTranslate(initXOffset, initYOffset)
            invalidate()
        }
    }

    override fun onDraw(canvas: Canvas) {
        bitmap?.let { bmp ->
            canvas.drawBitmap(bmp, transformationMatrix, null)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return scaleListener.onTouchEvent(event)
    }

    inner class ScaleListener : SimpleCustomOnScaleGestureListener() {
        private var focus = PointF()
        private var prevFocus = PointF()

        override fun onScaleBegin(detector: CustomScaleGestureDetector): Boolean {
            focus = PointF(detector.focusX, detector.focusY)
            prevFocus = focus

            return true
        }

        override fun onScaleEnd(detector: CustomScaleGestureDetector) {
            if(bitmap == null) {
                return
            }

            val matrixValues = transformationMatrix.values()

            val scale = matrixValues[Matrix.MSCALE_Y]

            val floatArr = floatArrayOf(0f, 0f)
            transformationMatrix.mapPoints(floatArr)
            val leftUpper = PointF(floatArr[0], floatArr[1])

            val viewCenterX = width / 2f
            val viewCenterY = height / 2f

            val imageCenterX = bitmap!!.width * scale / 2f + leftUpper.x
            val imageCenterY = bitmap!!.height * scale / 2f + leftUpper.y

            val xPivot =
                (viewCenterX - imageCenterX * initialScale / scale) / (1 - initialScale / scale)
            val yPivot =
                (viewCenterY - imageCenterY * initialScale / scale) / (1 - initialScale / scale)

            var prevScale = scale
            ValueAnimator.ofFloat(scale, initialScale).apply {
                interpolator = DecelerateInterpolator()
                addUpdateListener {
                    val newScale = it.animatedValue as Float

                    transformationMatrix.postScale(
                        newScale / prevScale,
                        newScale / prevScale,
                        xPivot,
                        yPivot
                    )

                    prevScale = newScale
                    invalidate()
                }
            }.start()
        }

        override fun onScale(detector: CustomScaleGestureDetector): Boolean {
            val matrixValues = transformationMatrix.values()
            val currentScale = matrixValues[Matrix.MSCALE_X]
            val scaleFactor = detector.scaleFactor

            val currentFocus = PointF(detector.focusX, detector.focusY)
            val focusVector = currentFocus - prevFocus
            prevFocus = currentFocus

            Log.d("TAG", "OnScale $currentScale $scaleFactor")

            // TODO: поменять на интерполяцию
            if(currentScale * scaleFactor > 3 * initialScale)
                return true
            if(currentScale * scaleFactor < 0.3 * initialScale)
                return true

            transformationMatrix.postScale(scaleFactor, scaleFactor, currentFocus.x, currentFocus.y)
            transformationMatrix.postTranslate(focusVector.x, focusVector.y)

            invalidate()

            return true
        }
    }
}