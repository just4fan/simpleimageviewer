package com.horrrnet.simpleimageviewer

import android.graphics.Matrix
import android.util.Size
import androidx.core.graphics.scaleMatrix
import androidx.core.graphics.times
import androidx.core.graphics.translationMatrix
import kotlin.math.min

fun initialScaleFactor(viewSize: Size, imageSize: Size): Float {
    val widthRatio = viewSize.width / imageSize.width.toFloat()
    val heightRatio = viewSize.height / imageSize.height.toFloat()

    return min(widthRatio, heightRatio)
}

fun scaleAndCenter(viewSize: Size, imageSize: Size): Matrix {
    val scaleFactor = initialScaleFactor(viewSize, imageSize)
    val xOffset = viewSize.width - imageSize.width * scaleFactor
    val yOffset = viewSize.height - imageSize.height * scaleFactor
    return translationMatrix(xOffset / 2, yOffset / 2) * scaleMatrix(scaleFactor, scaleFactor)
}