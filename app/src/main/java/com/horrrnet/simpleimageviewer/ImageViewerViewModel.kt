package com.horrrnet.simpleimageviewer

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ImageViewerViewModel(
    private val uri: Uri
) : ViewModel() {
    private var _bitmap: Bitmap? = null

    private fun loadBitmap(context: Context): Bitmap {
        context.contentResolver.openInputStream(uri).use {
            return BitmapFactory.decodeStream(it)
        }
    }

    suspend fun getBitmap(context: Context): Bitmap {
        withContext(Dispatchers.IO) {
            if (_bitmap == null) {
                _bitmap = loadBitmap(context)
            }
        }

        return checkNotNull(_bitmap)
    }
}

@Suppress("UNCHECKED_CAST")
class ImageViewerViewModelFactory(
    private val uri: Uri
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ImageViewerViewModel(uri) as T
    }
}